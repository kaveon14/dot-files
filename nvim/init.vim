set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath=&runtimepath

source ~/.config/nvim/config/vimrc.vim
source ~/.config/nvim/config/gruvbox.vim
source ~/.config/nvim/config/markdown-preview.vim
source ~/.config/nvim/config/nerd-tree.vim
source ~/.config/nvim/config/vim-plug.vim

colorscheme gruvbox
