map <C-n> :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen=1

function! IsNerdTreeEnabled()
    return exists('t:NERDTreeBufName') && bufeinnr(t:NERDTreeBufName) != -1
endfunction

map <space> <CR>
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
