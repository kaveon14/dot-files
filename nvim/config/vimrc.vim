" General Vim Settings

syntax on
syntax enable

set tabstop=2
set shiftwidth=2
set autoindent

map <space> <CR>

set nocompatible
filetype indent plugin on

set hidden
set autoindent
set wildmenu
set showcmd
set hlsearch
set ignorecase
set smartcase
 
" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
 
set nostartofline
set ruler
set laststatus=2
set confirm
set visualbell
 
" And reset the terminal code for the visual bell. If visualbell is set, and
" this line is also included, vim will neither flash nor beep. If visualbell
" is unset, this does nothing.
set t_vb=
 
" Enable use of the mouse for all modes
set mouse=a
 
set cmdheight=2
set number
set notimeout ttimeout ttimeoutlen=20
set pastetoggle=<F11>
map Y y$
nnoremap <C-L> :nohl<CR><C-L>
autocmd StdinReadPre * let s:std_in=1

set termguicolors

autocmd FileType markdown setlocal spell spelllang=en_us

set list
set listchars=tab:>-,trail:~,extends:>,precedes:<

" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
set expandtab

nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>

" OPen Quickfix after executing command(such as vimgrep) that populate the
" quickfix window
augroup qf
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost l*    cwindow
    autocmd VimEnter        *     cwindow
augroup END
