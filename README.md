# dotfiles

My .dotfiles

* Copy files to ~/.config
```
cp -r ~/path ~/.config
```

* Install VimPlug for neovim using the below command
```
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```
