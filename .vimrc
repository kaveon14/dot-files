execute pathogen#infect()
syntax on
syntax enable

set tabstop=2
set shiftwidth=2
set autoindent
set background=dark

let g:gruvbox_italice=0
let g:gruvbox_contrast_dark='soft'

set list
set listchars=tab:>-,trail:~,extends:>,precedes:<

map <C-n> :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen=1

function! IsNerdTreeEnabled()
    return exists('t:NERDTreeBufName') && bufeinnr(t:NERDTreeBufName) != -1
endfunction

autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

set nocompatible
filetype indent plugin on

set hidden
set wildmenu
set showcmd
set hlsearch
set ignorecase
set smartcase
 
" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
 
set nostartofline
set ruler
set laststatus=2
set confirm
set visualbell
 
" And reset the terminal code for the visual bell. If visualbell is set, and
" this line is also included, vim will neither flash nor beep. If visualbell
" is unset, this does nothing.
set t_vb=
 
" Enable use of the mouse for all modes
set mouse=a
 
set cmdheight=2
set number
set notimeout ttimeout ttimeoutlen=200
set pastetoggle=<F11>
map Y y$
nnoremap <C-L> :nohl<CR><C-L>
autocmd StdinReadPre * let s:std_in=1

set termguicolors
